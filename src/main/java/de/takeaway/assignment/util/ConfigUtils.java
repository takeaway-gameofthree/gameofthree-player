package de.takeaway.assignment.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigUtils.class);

    public static Properties properties = new Properties();

    public static String getString(String key) {
        return properties.getProperty(key).trim();
    }

    public static Integer getInt(String key) {
        Integer num = 0;
        try {
            num = Integer.parseInt(properties.getProperty(key).trim());

        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
        }

        return num;
    }

    public void loadConfigFile() {
        InputStream input = null;

        try {
            input = this.getClass().getClassLoader().getResourceAsStream("config.properties");
            properties.load(input);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }
    }

}
