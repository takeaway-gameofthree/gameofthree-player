package de.takeaway.assignment.util;

import de.takeaway.assignment.constant.Configs;

import java.util.Random;

/**
 * Util class for generating random number and calculate the game related number
 */
public class NumberUtil {

    public static Integer getRandomNumber() {
        return new Random().nextInt(Configs.RANDOM_NUMBER_BOUND);
    }

    public static Integer calculateNextNumber(Integer number) {
        int mod = number % 3;

        switch (mod) {
            case 1:
                number = (number - 1) / 3;
                break;
            case 0:
                number = number / 3;
                break;
            case 2:
                number = (number + 1) / 3;
                break;
        }

        return number;
    }
}
