package de.takeaway.assignment;

import de.takeaway.assignment.player.PlayerImpl;
import de.takeaway.assignment.util.ConfigUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        new ConfigUtils().loadConfigFile();
        String playerName;
        if (args.length != 0) {
            playerName = args[0];
        } else {
            playerName = String.valueOf(new Random().nextInt(4));
        }

        try {
            Thread.sleep(new Random().nextInt(2000));
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
        }

        PlayerImpl playerImpl = new PlayerImpl(playerName);
        playerImpl.initGame();
    }

}
