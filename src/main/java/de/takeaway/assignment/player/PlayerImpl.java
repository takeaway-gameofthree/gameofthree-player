package de.takeaway.assignment.player;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.takeaway.assignment.Config.DatabaseConfig;
import de.takeaway.assignment.Config.PlayerConnectionConfig;
import de.takeaway.assignment.constant.Configs;
import de.takeaway.assignment.enums.PlayerStatusEnum;
import de.takeaway.assignment.model.GamePlayMessage;
import de.takeaway.assignment.util.NumberUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.IOException;
import java.sql.SQLException;


/**
 * Player class
 */
public class PlayerImpl implements Player, MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerImpl.class);
    private static String PLAYER_NAME;
    private static String OPPONENT_NAME;
    private PlayerConnectionConfig connectionFactory = PlayerConnectionConfig.getInstance();
    private Boolean starter = false;
    private PlayerStatusEnum status = PlayerStatusEnum.READY;

    public PlayerImpl(String playerName) {
        PLAYER_NAME = playerName;
        new DatabaseConfig();
        init();
    }

    public Boolean getStarter() {
        return this.starter;
    }

    public void setStarter(Boolean stater) {
        this.starter = stater;
    }

    /**
     * Initialize the Listener
     */
    public void init() {
        try {
            startListening();
            LOGGER.info("PlayerImpl: " + PLAYER_NAME + " initialized.");
        } catch (JMSException e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Send message to queue name
     *
     * @param queueName
     * @param message
     * @throws JMSException
     */
    public void sendMessage(String queueName, String message) throws JMSException {
        Queue queue = connectionFactory.getSession().createQueue(Configs.QUEUE_PREFIX
                + queueName.toUpperCase());
        MessageProducer producer = connectionFactory.getSession().createProducer(queue);
        producer.send(connectionFactory.getSession().createTextMessage(message));
        LOGGER.info("Message send to Player: " + queueName + ". With content:" + message);
        producer.close();
    }

    /**
     * Bind class to be listener for incoming messages
     *
     * @throws JMSException
     */
    public void startListening() throws JMSException {
        LOGGER.info(PLAYER_NAME + " start listening on queue ->" + Configs.QUEUE_PREFIX + PLAYER_NAME.toUpperCase());
        Queue queue = connectionFactory.getSession().createQueue(Configs.QUEUE_PREFIX + PLAYER_NAME.toUpperCase());
        MessageConsumer consumer = connectionFactory.getSession().createConsumer(queue);
        consumer.setMessageListener(this);
    }

    /**
     * Listener Method for Player
     *
     * @param message
     */
    public void onMessage(Message message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            final String text = ((TextMessage) message).getText();
            GamePlayMessage gamePlayMessage = mapper.readValue(text, GamePlayMessage.class);

            //The second player send
            if (getStatus().equals(PlayerStatusEnum.READY)) {
                setStatus(PlayerStatusEnum.PLAYING);
            }
            LOGGER.info("Message received from " + gamePlayMessage.getPlayerName());
            LOGGER.info("Player :" + OPPONENT_NAME + " send number: " + gamePlayMessage.getNumber());
            if (getStatus().equals(PlayerStatusEnum.PLAYING)) {
                if (gamePlayMessage.getNumber() == 0) {
                    OPPONENT_NAME = gamePlayMessage.getPlayerName();
                    final int startNumber = NumberUtil.getRandomNumber();
                    sendMessage(OPPONENT_NAME, new GamePlayMessage(PLAYER_NAME, startNumber).toString());
                    LOGGER.info(PLAYER_NAME + " -> start game with " + OPPONENT_NAME + " - > number: " + startNumber);
                } else if (gamePlayMessage.getNumber() != 1) {
                    Integer num = NumberUtil.calculateNextNumber(gamePlayMessage.getNumber());
                    if (num != 1) {
                        LOGGER.info(PLAYER_NAME + "- >" + OPPONENT_NAME + " -> with number: " + num);
                        sendMessage(OPPONENT_NAME, new GamePlayMessage(PLAYER_NAME, num).toString());
                    } else {
                        LOGGER.info(PLAYER_NAME + " WIN. :) and " + OPPONENT_NAME + " Lose");
                        sendMessage(OPPONENT_NAME, new GamePlayMessage(PLAYER_NAME, num).toString());
                        setStatus(PlayerStatusEnum.WIN);
                    }
                } else {
                    LOGGER.info(OPPONENT_NAME + " Wins. :(( " + PLAYER_NAME + " Lose");
                    setStatus(PlayerStatusEnum.LOSE);
                }
            }
        } catch (JMSException e) {
            LOGGER.error(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

    }

    public PlayerStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PlayerStatusEnum status) {
        this.status = status;
    }

    /**
     * Initiate the game with checking if the other player joined the game
     */
    @Override
    public void initGame() {
        LOGGER.info("Player " + PLAYER_NAME + " waits for other Player to join.");
        try {
            if (!DatabaseConfig.isOtherPlayerExist()) {
                LOGGER.info("Player " + PLAYER_NAME + " is the starter.");
                setStarter(true);
            } else {
                //The second Player send message with Zero number to let the first player it ready to start the game
                String otherPlayerName = DatabaseConfig.otherPlayerName(PLAYER_NAME);
                OPPONENT_NAME = otherPlayerName;
                LOGGER.info("Player " + OPPONENT_NAME + " will start first.");
                sendMessage(otherPlayerName, new GamePlayMessage(PLAYER_NAME, 0).toString());
            }
            DatabaseConfig.savePlayer(PLAYER_NAME);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        } catch (JMSException e) {
            LOGGER.error(e.getMessage());
        }

    }
}
