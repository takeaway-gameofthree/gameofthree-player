package de.takeaway.assignment.player;

import javax.jms.JMSException;

public interface Player {

    void sendMessage(String queueName, String message) throws JMSException;

    void startListening() throws JMSException;

    void initGame();
}
