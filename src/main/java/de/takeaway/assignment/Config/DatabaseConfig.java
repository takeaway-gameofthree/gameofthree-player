package de.takeaway.assignment.Config;

import de.takeaway.assignment.constant.Configs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class DatabaseConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseConfig.class);

    private static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(Configs.DB_DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(Configs.DB_URL, "", "");
            return dbConnection;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        return dbConnection;
    }

    public static void savePlayer(String playerName) throws SQLException {
        Connection connection = getDBConnection();
        PreparedStatement preparedStatement = null;
        String insertQuery = "INSERT INTO PLAYER (name,queue) values (?,?)";
        try {
            connection.setAutoCommit(true);

            preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setString(1, playerName);
            preparedStatement.setString(2, playerName);
            preparedStatement.executeUpdate();
            preparedStatement.close();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            connection.close();
        }
    }

    public static Boolean isOtherPlayerExist() throws SQLException {
        Connection connection = getDBConnection();
        PreparedStatement preparedStatement = null;
        int playerNum = 0;
        String selectQuery = "SELECT count(*) as count FROM PLAYER";

        try {
            preparedStatement = connection.prepareStatement(selectQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                playerNum = rs.getInt("count");
            }
            preparedStatement.close();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            connection.close();
        }
        return playerNum > 0;
    }

    public static String otherPlayerName(String myName) throws SQLException {
        Connection connection = getDBConnection();
        PreparedStatement preparedStatement = null;
        String playerName = "";
        String selectQuery = "SELECT name FROM PLAYER where name <> '" + myName + "'";

        try {
            preparedStatement = connection.prepareStatement(selectQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                playerName = rs.getString(1);
            }
            preparedStatement.close();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            connection.close();
        }
        return playerName;
    }
}
