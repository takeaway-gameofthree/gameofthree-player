package de.takeaway.assignment.Config;

import de.takeaway.assignment.constant.Configs;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * Responsible for setup connection to ActiveMQ
 */
public class PlayerConnectionConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerConnectionConfig.class);
    private static PlayerConnectionConfig instance;
    private ConnectionFactory connectionFactory;
    private Session session;

    public PlayerConnectionConfig() {
        connectionFactory = new ActiveMQConnectionFactory(Configs.ACTIVEMQ_URL);
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (JMSException e) {
            LOGGER.error("Could not start te Coordinator");
        }
    }

    public static synchronized PlayerConnectionConfig getInstance() {
        if (instance == null) {
            instance = new PlayerConnectionConfig();
        }
        return instance;
    }

    public ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
