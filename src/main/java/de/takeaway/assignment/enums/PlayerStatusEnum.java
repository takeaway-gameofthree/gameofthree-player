package de.takeaway.assignment.enums;

public enum PlayerStatusEnum {
    READY,
    PLAYING,
    WIN,
    LOSE
}
