# gameofthree-player

Player agent for Takeaway assignment

# How to start
Before this you should get gameofthree-server form 
[this link](git@gitlab.com:takeaway-gameofthree/gameofthree-server.git) and run:
```console
 mvn package
```
and then start the server  with

```console
java -jar target\gameoftree-1.0-SNAPSHOT.jar
```
and then you should run
```console
 mvn package
```
for project [player](git@gitlab.com:takeaway-gameofthree/gameofthree-player.git) and run jar file with player name:

```console
java -jar target\player-1.0-SNAPSHOT.jar [player name]
```


