package de.takeaway.assignment.model;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GamePlayMessage {

    private String playerName;
    private int number;

    public GamePlayMessage() {
    }

    public GamePlayMessage(String playerName, int number) {
        this.playerName = playerName;
        this.number = number;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
