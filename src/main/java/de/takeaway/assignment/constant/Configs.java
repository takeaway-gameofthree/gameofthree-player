package de.takeaway.assignment.constant;

import de.takeaway.assignment.util.ConfigUtils;

public class Configs {

    public static final String ACTIVEMQ_URL = ConfigUtils.getString("de.takeaway.assignment.activemq.url");
    public static final String DB_URL = ConfigUtils.getString("de.takeaway.assignment.db.url");
    public static final String DB_DRIVER_CLASS = ConfigUtils.getString("de.takeaway.assignment.db.driver.class");
    public static final String QUEUE_PREFIX = ConfigUtils.getString("de.takeaway.assignment.queue.prefix");
    public static final Integer RANDOM_NUMBER_BOUND = ConfigUtils.getInt("de.takeaway.assignment.random.number.bound");
    public static final Integer RANDOM_DELAY_TO_START = ConfigUtils.getInt("de.takeaway.assignment.random.delay.to.start");
}
